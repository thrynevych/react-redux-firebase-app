export const createProject = (project) => {
    return (dispatch, getState, {getFirebase, getFirestore}) => {
        const firestore = getFirestore();
        firestore.collection('projects').add({
            ...project,
            authorFirstName: 'TestFirstName',
            authorLastName: 'TestLasName',
            timestamp: new Date(),
            authorId: 12345
        }).then((res) => {
            console.log(res);
            project.id = res.id;
            dispatch({type: 'ADD_PROJECT', project});
        }).catch((err) => {
            dispatch({type: 'ADD_PROJECT_ERROR', err});
        });
    }
};
