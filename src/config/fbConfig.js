import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

// Initialize Firebase
var config = {
    apiKey: "AIzaSyAQjqjoYh_NFMVP1hCs-xb0C1990AOlwxc",
    authDomain: "net-ninja-marioplan-8912b.firebaseapp.com",
    databaseURL: "https://net-ninja-marioplan-8912b.firebaseio.com",
    projectId: "net-ninja-marioplan-8912b",
    storageBucket: "net-ninja-marioplan-8912b.appspot.com",
    messagingSenderId: "920024211201"
};

firebase.initializeApp(config);
firebase.firestore().settings({timestampsInSnapshots: true});

export default firebase;